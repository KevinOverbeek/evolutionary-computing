"""
Purpose:
    Creating a specialist EA

Version:
    1 - Initial version

Date:
    2019.09.20

Author:
    Kevin Overbeek, Thijmen van Arnhem, Chris Kostopoulos
"""

for mutMethod in ["SA_OneStep", "SA_NStep"]:
    for crossMethod in ["N-point", "Blend"]:
        for selectionMethod in ["Lin_rank", "Tournament"]:
            
# In[Imports, setting up environment]

            # Set working directory
            import os
            if os.getlogin() == "Kevin Overbeek":
                os.chdir('D:/Kevin Overbeek/Documents/Studie/2019-20 BA/6.1 Evolutionary Computing/Bitbucket')
            elif os.getlogin() == "thijm":
                os.chdir('C:/EvC/Source')
            elif os.getlogion() == "chris":
                os.chdir('C:/Users/chris/Google Drive/Master/Courses/Evolutionary Computing/Assignment')
            
            # Import framework, 
            import sys
            sys.path.insert(0, 'evoman')
            from environment import Environment
            from demo_controller import player_controller
            
            # Import standard python packages & custom made functions
            import time #analysis:ignore
            import numpy as np #analysis:ignore
            import pandas as pd #analysis:ignore
            import csv #analysis:ignore
            import Assignment_functions as fn #analysis:ignore
            
            # Make folder to write experiments to
            experiment_name = 'Assignment_generalist'
            if not os.path.exists(experiment_name):
                os.makedirs(experiment_name)
            Logpath = os.getcwd() + '\\' + experiment_name
            
            # Make folder, see if it exists
            Logpath += '\\' + mutMethod + '_' + crossMethod + '_' + selectionMethod + '\\'
            if not os.path.exists(Logpath):
                os.makedirs(Logpath)
                
                # Initializes simulation in individual evolution mode, for single static enemy.
                env = Environment(experiment_name = experiment_name,
                                  enemies = [1,2,4,7],
                                  multiplemode="yes",
                                  playermode = "ai",
                                  player_controller = player_controller(),
                                  enemymode = "static",
                                  level = 2,
                                  speed = "fastest")
                
                # Print environment state
                env.state_to_log()
    
    # In[Parameter setting]
    
                # Choose to start a new run / load previous, choose to show best solution so far
                Newrun = 'Y' # 'Y' or 'N'
                Showbest = 'N' # 'Y' or 'N'
                
                # Set start time and random seed
                StartTime = time.time()  # sets time marker
                Randseed = 0
                np.random.seed(Randseed)
                
                # General parameters
                N_generations = 5
                N_rotations = 10
                N_exchanges = 2
                Mu = 10
                Lambda = 40
                N_nodes = 10
                Outputs = 5
                
                # Make dictionaries for every island
                Island1 = {"Mu": Mu, "Lambda": Lambda, "N_nodes": N_nodes, "Outputs": Outputs}
                Island2 = {"Mu": Mu, "Lambda": Lambda, "N_nodes": N_nodes, "Outputs": Outputs}
                Island3 = {"Mu": Mu, "Lambda": Lambda, "N_nodes": N_nodes, "Outputs": Outputs}
                Island4 = {"Mu": Mu, "Lambda": Lambda, "N_nodes": N_nodes, "Outputs": Outputs}
                Island5 = {"Mu": Mu, "Lambda": Lambda, "N_nodes": N_nodes, "Outputs": Outputs}
                
                # In[Setup Island 1]
                
                # Setup metaparameters
                Island1["MethodMutation"] = mutMethod
                Island1["MethodCrossover"] = crossMethod
                Island1["MethodSurvivorProb"] = selectionMethod
                Island1["MethodSurvivorSelection"] = "Roulette"
                Island1["MethodParentProb"] = selectionMethod
                Island1["MethodParentSelection"] = "Roulette" 
                Island1["Elitism"] = 'Y'
                
                # EA parameters
                Island1["S"] = 2
                Island1["N_parents"] = 6
                Island1["MutationRate"] = 0.2
                Island1["CrossoverRate"] = 0.8
                Island1["CrossoverNumber"] = 6
                Island1["Sigma"] = 1
                Island1["Eps"] = 0.1
                Island1["q"] = 6
                Island1["Alpha"] = 0.5
                
                Island1 = fn.Setup_Island(Island1, env, Logpath, 1)
                
                # In[Setup Island 2]
                
                # Setup metaparameters
                Island2["MethodMutation"] = mutMethod
                Island2["MethodCrossover"] = crossMethod
                Island2["MethodSurvivorProb"] = selectionMethod
                Island2["MethodSurvivorSelection"] = "Roulette"
                Island2["MethodParentProb"] = selectionMethod
                Island2["MethodParentSelection"] = "Roulette"
                Island2["Elitism"] = 'Y'
                
                # EA parameters
                Island2["S"] = 1.25
                Island2["N_parents"] = 4
                Island2["MutationRate"] = 0.05
                Island2["CrossoverRate"] = 0.7
                Island2["CrossoverNumber"] = 6
                Island2["Sigma"] = 1
                Island2["Eps"] = 0.01
                Island2["q"] = 4
                Island2["Alpha"] = 0.5
                
                Island2 = fn.Setup_Island(Island2, env, Logpath, 2)
                
                # In[Setup Island 3]
                
                # Setup metaparameters
                Island3["MethodMutation"] = mutMethod
                Island3["MethodCrossover"] = crossMethod
                Island3["MethodSurvivorProb"] = selectionMethod
                Island3["MethodSurvivorSelection"] = "Roulette"
                Island3["MethodParentProb"] = selectionMethod
                Island3["MethodParentSelection"] = "Roulette"
                Island3["Elitism"] = 'Y'
                
                # EA parameters
                Island3["S"] = 1.5
                Island3["N_parents"] = 2
                Island3["MutationRate"] = 0.1
                Island3["CrossoverRate"] = 0.6
                Island3["CrossoverNumber"] = 4
                Island3["Sigma"] = 1
                Island3["Eps"] = 0.05
                Island3["q"] = 8
                Island3["Alpha"] = 0.5
                
                Island3 = fn.Setup_Island(Island3, env, Logpath, 3)
                
                # In[Setup Island 4]
                
                # Setup metaparameters
                Island4["MethodMutation"] = mutMethod
                Island4["MethodCrossover"] = crossMethod
                Island4["MethodSurvivorProb"] = selectionMethod
                Island4["MethodSurvivorSelection"] = "Roulette"
                Island4["MethodParentProb"] = selectionMethod
                Island4["MethodParentSelection"] = "Roulette"
                Island4["Elitism"] = 'Y'
                
                # EA parameters
                Island4["S"] = 1.25
                Island4["N_parents"] = 4
                Island4["MutationRate"] = 0.1
                Island4["CrossoverRate"] = 0.55
                Island4["CrossoverNumber"] = 2
                Island4["Sigma"] = 1
                Island4["Eps"] = 0.01
                Island4["q"] = 2
                Island4["Alpha"] = 0.5
                
                Island4 = fn.Setup_Island(Island4, env, Logpath, 4)
                
                # In[Setup Island 5]
                
                # Setup metaparameters
                Island5["MethodMutation"] = mutMethod
                Island5["MethodCrossover"] = crossMethod
                Island5["MethodSurvivorProb"] = selectionMethod
                Island5["MethodSurvivorSelection"] = "Roulette"
                Island5["MethodParentProb"] = selectionMethod
                Island5["MethodParentSelection"] = "Roulette"
                Island5["Elitism"] = 'Y'
                
                # EA parameters
                Island5["S"] = 2
                Island5["N_parents"] = 6
                Island5["MutationRate"] = 0.2
                Island5["CrossoverRate"] = 0.7
                Island5["CrossoverNumber"] = 2
                Island5["Sigma"] = 1
                Island5["Eps"] = 0.1
                Island5["q"] = 4
                Island5["Alpha"] = 0.5
                                
                Island5 = fn.Setup_Island(Island5, env, Logpath, 5)
                
                # In[Initialize]
                
                Island1 = fn.Initialise_Island(Island1, env)
                Island2 = fn.Initialise_Island(Island2, env)
                Island3 = fn.Initialise_Island(Island3, env)
                Island4 = fn.Initialise_Island(Island4, env)
                Island5 = fn.Initialise_Island(Island5, env)
                
                # In[Run generations]
                
                for i in range(N_rotations):
                    
                    # Evaluate every island
                    Island1 = fn.Evolve_Island(Island1, env, N_generations)
                    Island2 = fn.Evolve_Island(Island2, env, N_generations)
                    Island3 = fn.Evolve_Island(Island3, env, N_generations)
                    Island4 = fn.Evolve_Island(Island4, env, N_generations)
                    Island5 = fn.Evolve_Island(Island5, env, N_generations)
                    
                    # Rotate
                    Island1, Island2, Island3, Island4, Island5 = fn.Exchange_Islands(Island1, Island2, Island3, Island4, Island5, N_exchanges)
    
# In[Run best solution]            

# Get maximum of every island
#Island = Island1
#for IslandTemp in [Island2, Island3, Island4, Island5]:
#    if max(Island["Max_fitness_over_time"]) < max(IslandTemp["Max_fitness_over_time"]):
#        Island = IslandTemp



## Beware: crashes after it completes, deletes all variables in memory
#if Showbest == 'Y':
#    Best = np.loadtxt(Logpath + '\\Best_solution.txt')
#    env.update_parameter('speed','normal')
#    fn.Evaluate_population(Best.reshape(1, len(Best)), env, NN_variables)