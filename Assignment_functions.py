# -*- coding: utf-8 -*-
"""
Purpose:
    
    
Inputs:
    
    
Outputs:
    
    
Version history:
    1.  
"""

# In[Imports]

import numpy as np 
import bisect
import csv
import os

# In[Evaluation functions]

"""
Purpose:
    Calculate the fitness values for a population
    
Inputs:
    Population: Population of individuals, accepted by environment
    Environment: evoman environment
    NN_variables: Number of variables of the neural network
    
Outputs:
    Fitness: numpy array with fitness values
    
Version history:
    1. Kevin, 2019/09/27: Created the function
"""

def Evaluate_population(population, environment, NN_variables):
    
    # Determine population size
    N_pop = population.shape[0]
    Fitness = np.zeros(N_pop)
    Playerlife = np.zeros(N_pop)
    
    for i in range(N_pop):
        Fitness[i], Playerlife[i], _, _ = environment.play(population[i,:NN_variables])
    
    return Fitness, Playerlife

# In[Selection functions]
    
"""
Purpose:
    Calculating the linear rank of a set of individuals

Inputs:
    Fitness: numpy array with fitness values
    S: Linear rank model parameter
    
Outputs:
    Probability: numpy array with probabilities of being selected
    
Version history:
    1. Kevin, 2019/09/27: Created the function  
"""

def Lin_rank(Fitness, S):
    
    # Determine population size
    N_pop = len(Fitness)
    
    # Get order of fitness, set up empty rank and probability vectors
    Order = np.argsort(Fitness)    
    Rank = np.zeros(N_pop)
    Probability = np.zeros(N_pop)
    
    # For every individual, determine rank and calculate probability
    for i in range(N_pop):
        IndividualRank = Order[i]
        Rank[IndividualRank] = i
        Probability[IndividualRank] = (2-S)/N_pop + 2*i*(S-1)/ (N_pop*(N_pop-1))
    
    assert round(Probability.sum(),10) == 1
    
    return Probability

"""   
Purpose:
    Calculating the exponential rank of a set of individuals

Inputs:
    Fitness: numpy array with fitness values
    
Outputs:
    Probability: numpy array with probabilities of being selected
    
Version history:
    1. Thijmen, 2019/09/28: Created the function  
"""

def Exp_rank(Fitness):
    
    # Determine population size
    N_pop = len(Fitness)
    
    # Get order of fitness, set up empty rank and probability vectors
    Order = np.argsort(Fitness)    
    Rank = np.zeros(N_pop)
    Probability = np.zeros(N_pop)
    
    # Calculate normalisation constant
    Normalisation = 0
    for j in range(1,N_pop):
        Normalisation += (1-np.exp(-j))
    
    # For every individual, determine rank and calculate probability
    for i in range(N_pop):
        IndividualRank = Order[i]
        Rank[IndividualRank] = i
        Probability[IndividualRank] = (1-np.exp(-i)) / Normalisation
    
    assert round(Probability.sum(),10) == 1
    
    return Probability

"""

Purpose:
    Selecting individuals (duplicates allowed) from the population using Tournament selection

Inputs:
    Fitness: numpy array with fitness values
    N_tournaments: Number of tournaments to be played
    q: Number of participants that will face off each round.
    
Outputs:
    Probability: numpy array with probabilities of being selected
    
Version history:
    1. Thijmen, 2019/09/28: Created the function  
    2. Kevin, 2019/09/29: Incorporated function in Get_probabilities()
    3. Kevin, 2019/09/30: Renamed arguments
"""

def Tournament(N_tournaments, q, Fitness):
    
    # Make empty vector with wins
    Wins = np.zeros([len(Fitness)])
    N_pop = len(Fitness)

    # Hold N_individuals competitions
    for i in range(0, N_tournaments):
        
        # Randomly select q participants
        Participants = []
        for j in range(0, q): 
            Participants += [int(np.floor(np.random.rand() * N_pop))]
            
        # Retrieve index to winner, add win to vector
        idx = [Participants[np.argmax([Fitness[k] for k in Participants])]] 
        Wins[idx] += 1

    # Convert wins into probabilities
    Probability = Wins / N_tournaments
    
    assert round(Probability.sum(),10) == 1
    
    return Probability

"""
Purpose:
    Calculating parent / survival probabilities based on chosen method
    
Inputs:
    Fitness: Vector with fitness values
    Method: Method chosen to calculate probabilities, choose from:
        1. Lin_rank: Linear ranking method
        2. Exp_rank: Exponential ranking method
        3. Tournament: Tournament selection
    S: (Optional), model parameter for Lin_rank
    N_individuals: (Optional), Number of tournaments to play
    q: (Optional), Number of participants in each tournament
    
Outputs:
    Probabilities: Vector with probabilities
    
Version history:
    1. Kevin, 2019/09/27: Created the function
    2. Kevin, 2019/09/29: Added exponential rank and tournament
"""

def Get_probabilities(Fitness, Method, S = None, N_individuals = None, q = None):
    
    # Calculate probabilities based on chosen method
    if Method == "Lin_rank":
        Probabilities = Lin_rank(Fitness, S)
    elif Method == "Exp_rank":
        Probabilities = Exp_rank(Fitness)
    elif Method == "Tournament":
        Probabilities = Tournament(N_individuals, q, Fitness)
    
    return Probabilities

"""
Purpose:
    Select N individuals based on probabilities, roulette wheel with equally spaced arms
    
Inputs:
    Probabilities: numpy array with probabilities
    N_individuals: Number of individuals to be selected
    
Outputs:
    idx: Index number of selected individuals
    
Version history:
    1. Kevin, 2019/09/27: Created the function  
"""

def Roulettewheel_selection(Probabilities, N_individuals):
    
    # Determine cumulative distribution        
    cdf = []
    Total = 0
    for p in range(len(Probabilities)-1):
        Total += Probabilities[p]
        cdf.append(Total)
    
    # Force last entry to be 1
    cdf.append(1)
    
    # Select action based on cdf
    x = np.random.rand() / N_individuals
    idx = []
    for i in range(N_individuals):
        idx.append(bisect.bisect(cdf, i/N_individuals + x))
    
    return idx

"""
Purpose:
    Selecting individuals based on chosen method
    
Inputs:
    Probabilities: Numpy array with probabilities of being chosen
    Method: Method chosen for selection procedure, choose from:
        Roulette: Roulette wheel selection which selects N_individuals per time
    N_individuals: Number of individuals to be selected at once
    
Outputs:
    Index: Index numbers of selected individuals
    
Version history:
    1. Kevin, 2019/09/27: Created the function  
"""


def Select_individuals(Probabilities, Method, N_individuals):
    
    if Method == "Roulette":
        Index = Roulettewheel_selection(Probabilities, N_individuals)
    elif Method == "":
        Index = 1 # Add some other method
    
    return Index

"""
Purpose:
    Apply elitism (i.e. keeping best one in)
    
Inputs:
    Population: Current set of selected individuals
    Fitness_population: Fitness values of selected individuals
    Playerlife_population: Playerlife values of selected individuals
    Offspring: Full set of generated offspring, from which population is picked
    Fitness_offspring: Fitness values of set of offspring
    Playerlife_offspring: Playerlife values of set of offspring
    Elitism: Parameter to choose whether or not to apply elitism
    Max_fitness_over_time: List with maximum fitness values of each generation
    Logpath: Path where all the logfiles are stored
    
Outputs:
    Population: Population with, if absent, best individual placed in it
    Fitness_population: Fitness values of population
    
Version history:
    1. Kevin, 2019,10,01: Created the functions
"""

def Apply_elitism(Population, Fitness_population, Playerlife_population, Offspring, Fitness_offspring, Playerlife_offspring, Elitism, Max_fitness_over_time, Logpath):

    # Check if Elitism applies    
    if Elitism == 'Y':
    
        # Get maximum historical fitness and best of offspring
        MaxFitPop = Fitness_population.max()
        MaxFitOff = Fitness_offspring.max()
        
        # Check if somebody in current generation is better than best all time
        if MaxFitOff < max(Max_fitness_over_time):
            
            # Load best solution and fitness
            Best = np.loadtxt(Logpath + '\\Best_solution.txt')
            BestFit = max(Max_fitness_over_time)
            BestLife = np.loadtxt(Logpath + '\\Best_solution_playerlife.txt')
            
            # Pick random index to be replaced
            RandomIdx = int(np.random.rand() * Population.shape[0])
            
            # Place best solution in this index
            Population[RandomIdx,:] = Best
            Fitness_population[RandomIdx] = BestFit
            Playerlife_population[RandomIdx] = BestLife
            
        elif MaxFitOff > MaxFitPop:
            
            # Retrieve best solution and fitness
            Best = Offspring[np.argmax(Fitness_offspring),:]
            BestFit = Fitness_offspring[np.argmax(Fitness_offspring)]
            BestLife = Playerlife_offspring[np.argmax(Fitness_offspring)]
            
            # Pick random index to be replaced
            RandomIdx = int(np.random.rand() * Population.shape[0])
            
            # Place best solution in this index
            Population[RandomIdx,:] = Best
            Fitness_population[RandomIdx] = BestFit
            Playerlife_population[RandomIdx] = BestLife
            
    return Population, Fitness_population, Playerlife_population

# In[Creating offspring]

"""
Purpose:
    Select a subset of individuals from the total population
    
Inputs:
    Population: numpy array with complete population
    Selected: List with selected individuals from total population
    
Outputs:
    SelectedPopulation: numpy array with only selected members from population
    
Version history:
    1. Kevin, 2019/09/27: Created the function  
"""

def Retrieve_individuals(Population, Selected):
    
    SelectedPopulation = np.zeros([len(Selected), Population.shape[1]])
    
    for i in range(0, len(Selected)):
        IndividualTemp = Selected[i]
        SelectedPopulation[i,:] = Population[IndividualTemp,:]

    return SelectedPopulation

"""
Purpose:
    Select a subset of fitness values from the total population
    
Inputs:
    Population: numpy array with fitness values
    Selected: List with selected individuals from total population
    
Outputs:
    SelectedFitness: numpy array with only selected fitness values from population
    
Version history:
    1. Kevin, 2019/09/27: Created the function  
"""

def Retrieve_fitness(Fitness, Selected):
    
    SelectedFitness = np.zeros(len(Selected))
    
    for i in range(0, len(Selected)):
        IndividualTemp = Selected[i]
        SelectedFitness[i] = Fitness[IndividualTemp]

    return SelectedFitness     

"""
Purpose:
    Perform N-point crossover on a set of individuals
    
Inputs:
    SelectedPopulation: Set of individuals on which crossover is applied
    CrossoverRate: Fraction of parents on which crossover is applied
    CrossoverNumber: Number of crossover points
    
Outputs:
    Offspring: Created offspring
    
Version history:
    1. Kevin, 2019/09/27: Created the function  
    2. Kevin, 2019/09/30: Added Crossover rate
"""

def N_point_crossover(SelectedPopulation, CrossoverRate, CrossoverNumber):
    
    # Make dataframe with offspring
    Offspring = SelectedPopulation.copy()
    
    # Randomly determine if crossover is performed
    if np.random.rand() < CrossoverRate:
    
        # Determine dimensions of offspring / parents
        N_Parents = SelectedPopulation.shape[0]
        N_Parameters = SelectedPopulation.shape[1]
        
        # Make empty arrays for crossover intervals and crossover points
        AverageIntervalLength = (N_Parameters - 1) / CrossoverNumber
        CrossoverPointsIntervals = np.zeros(CrossoverNumber+1)
        
        CrossoverPoints = np.zeros(CrossoverNumber+2)
        CrossoverPoints[-1] = N_Parameters
        
        # Fill crossover intervals
        for i in range(0, CrossoverNumber+1):
            CrossoverPointsIntervals[i] = np.floor(i * AverageIntervalLength)
        
        # Fill crossover points
        for i in range(1, CrossoverNumber+1):
            CrossoverPoints[i] = np.ceil(np.random.rand() * (CrossoverPointsIntervals[i] - CrossoverPointsIntervals[i-1])) + CrossoverPointsIntervals[i-1]
            
        # Create offspring by recombining segments of parents
        for n in range(0, CrossoverNumber + 1):    
            LB = int(CrossoverPoints[n])
            UB = int(CrossoverPoints[n+1])
            
            for i in range(0, N_Parents):
                    Offspring[i,LB:UB] = SelectedPopulation[((i+n) % N_Parents),LB:UB]
                
    return Offspring

"""
Purpose:
    Perform blend crossover on a pair of individuals
    
Inputs:
    SelectedPopulation: Set of individuals on which crossover is applied
    CrossoverRate: Fraction of parents on which crossover is applied
    Alpha: Model parameter, determines the magnitude of the change
    
Outputs:
    Offspring: Created offspring
    
Version history:
    1. Kevin, 2019/10/01: Created the function
"""

def Blend_crossover(SelectedPopulation, CrossoverRate, Alpha):
    
    # Check if we exactly have 2 parents
    assert SelectedPopulation.shape[0] == 2
    
    # Make dataframe with offspring
    Offspring = SelectedPopulation.copy()
    
    # Randomly determine if crossover is performed
    if np.random.rand() < CrossoverRate:
        
        # Loop over every gene
        for i in range(Offspring.shape[1]):
            
            # Calculate difference, add this randomly to allele
            Diff = np.abs(Offspring[0,i] - Offspring[1,i])
            Offspring[0,i] += ((np.random.rand() * 2 * Alpha) - Alpha) * Diff
            Offspring[1,i] += ((np.random.rand() * 2 * Alpha) - Alpha) * Diff
    return

"""
Purpose:
    Choosing and applying recombination operators
    
Inputs:
    SelectedPopulation: Set of individuals on which recombination is applied 
    MethodCrossover: Chosen crossover method, choose from:
        N-point: Standard N-point crossover, where crossover points are split over equally sized intervals
        Blend: Blend crossover (i.e. add constant * difference between parents)
    CrossoverRate: Fraction of parents that do crossover
    CrossoverNumber: (Optional), Argument for n-point crossover
    Alpha: (Optional), Argument for blend crossover
    MethodMutation: Chosen mutation method, choose from:
        Uniform: Add a standard uniform number - 0.5
        Gaussian: Add a standard normal number
        SA_OneStep: Add a normally distributed number with self-adepting step size
        SA_NStep: Add a normally distributed number with n self-adepting step sizes
    MutationRate: Fraction of genes that are mutated
    Sigma: (Optional), step size, used in Gaussian mutation
    Eps: (Optinal), the minimum standard deviation for OneStep and NStep mutation
    
Outputs:
    Offspring: Generated set of offspring
    
Version history:
    1. Kevin, 2019/09/27: Created the function  
    2. Thijmen, 2019/09/29: Added Onestep and NStep Mutation. To be verified
    3. Kevin, 2019/09/30: Fully incorporated OneStep and NStep
"""

def Apply_recombination(SelectedPopulation, MethodCrossover = None, CrossoverNumber = None, CrossoverRate = None, Alpha = None, MethodMutation = None, MutationRate = None, Sigma = None, Eps = None):
    
    #############################
    # Apply crossover operators #
    #############################
    
    if MethodCrossover == "N-point":
        Offspring = N_point_crossover(SelectedPopulation, CrossoverRate, CrossoverNumber)
        
    elif MethodCrossover == "Blend":
        Offspring = Blend_crossover(SelectedPopulation, CrossoverRate, Alpha)
        
    else: # if no crossover is performed, keep original
        Offspring = SelectedPopulation.copy()

    ############################
    # Apply mutation operators #
    ############################
    
    if MethodMutation == "Uniform":
        for i in range(0, Offspring.shape[0]):
            for j in range(0, Offspring.shape[1]):
                if np.random.rand() < MutationRate:
                    Offspring[i,j] += np.random.rand() - 0.5
    
    elif MethodMutation == "Gaussian":        
         for i in range(0, Offspring.shape[0]):
             for j in range(0, Offspring.shape[1]):
                 if np.random.randn() < MutationRate:
                     Offspring[i,j] += np.random.randn() * Sigma
    
    elif MethodMutation == "SA_OneStep":
        
        # Calculate step size of sigma, same between individuals
        Tau = 1 / (np.sqrt(Offspring.shape[1]))
        
        # Loop over every individual
        for i in range(0, Offspring.shape[0]):
            
            # Update step size parameter
            SigmaOld = Offspring[i,-1]; #The old standard deviation
            SigmaNew = np.clip(SigmaOld * np.exp(Tau * np.random.randn()), a_min = Eps, a_max = None) #The new standard deviation
            Offspring[i,-1] = SigmaNew; #Overwrite SigmaOld from the gene by SigmaNew

            # Update all other variables with new step size parameter
            for j in range(0, Offspring.shape[1] - 1):
                if np.random.randn() < MutationRate:
                    Offspring[i,j] += SigmaNew * np.random.randn()
    
    elif MethodMutation == "SA_NStep":
        
        # Determine Tau and Tau Prime
        N_Genes = Offspring.shape[1]
        TauPrime = 1 / (np.sqrt(2 * N_Genes)) #Create tau prime
        Tau = 1 / (np.sqrt(2 * np.sqrt(N_Genes)))  #Create tau
        
        # Loop over every individual
        for i in range(0, Offspring.shape[0]):
            
            # Determine constant factor of sigma update
            factor1 = np.exp(TauPrime * np.random.randn())
            
            # Loop over every gene
            for j in range(0, int(N_Genes/2)):
                
                # Calculate sigma specific change, update sigma
                factor2 = np.exp(Tau * np.random.randn())
                SigmaOld = Offspring[i,int(N_Genes/2+j)]
                SigmaNew = np.clip((SigmaOld * factor1 * factor2), a_min = Eps, a_max = None)

                # Update gene and place back SigmaNew
                if np.random.randn() < MutationRate:
                    Offspring[i,int(N_Genes/2+j)] = SigmaNew
                    Offspring[i,j] += SigmaNew * np.random.randn()
    
    return Offspring

"""
Purpose:
    Select N_parents from list of indices
    
Inputs:
    Index: List with parents
    N_parents: Number of parents to select from list
    
Outputs:
    IndexNew: List with parents without the chosen ones
    Parents: Chosen parents
    
Version history:
    1. Kevin, 2019/09/27: Created the function
"""

def Select_parents(Index, N_parents):
    
    # Determine number of available parents, check if sufficient
    N = len(Index)
    if N < N_parents:
        print("Number of potential parents insufficient (list with %i parents given, %i parents requested)" % (N, N_parents))
        return
    
    # Select parents from list
    IndexNew = Index.copy()
    Parents = []
    for i in range(0, N_parents):
        IndexTemp = int(np.random.random() * (N - i))
        Parents.append(IndexNew[IndexTemp])
        del IndexNew[IndexTemp]
        
    return IndexNew, Parents

"""
Purpose:
    Create offspring
    
Inputs:
    Population: Current population
    ParentList: List with index numbers of parents
    N_parents: Number of parents to select per crossover /  mutation
    MethodCrossover: Chosen crossover method, choose from:
        N-point: Standard N-point crossover, where crossover points are split over equally sized intervals
        Blend: Blend crossover (i.e. add constant * difference between parents)
    CrossoverNumber: (Optional), Argument for n-point crossover
    Alpha: (Optional), Argument for blend crossover
    MethodMutation: Chosen mutation method, choose from:
        Uniform: Add a standard uniform number - 0.5
        Gaussian: Add a standard normal number
        SA_OneStep: Add a normally distributed number with self-adepting step size
        SA_NStep: Add a normally distributed number with n self-adepting step sizes
    MutationRate: Fraction of genes that are mutated
    Sigma: (Optional), step size, used in Gaussian mutation
    Eps: (Optinal), the minimum standard deviation for OneStep and NStep mutation
    
Outputs:
    Offspring: Set of generated offspring
    
Version history:
    1. Kevin, 2019/09/27: Created the function
    2. Kevin, 2019/09/30: Added optional arguments, updated docstring
    3. Kevin, 2019/10/04: Incorporated mistake spotted by Thijmen
"""

def Get_offspring(Population, ParentList, N_parents, MethodCrossover = None, CrossoverNumber = None, CrossoverRate = None, Alpha = None, MethodMutation = None, MutationRate = None, Sigma = None, Eps = None):
    
    # Make empty matrix with offspring
    Offspring = np.zeros([len(ParentList), Population.shape[1]])
    
    # Number of pairs
    N_pairs = int(len(ParentList) / N_parents)
    
    for i in range(0, N_pairs):
        
        # Select 2 random parents, remove from list
        ParentList, Selected = Select_parents(ParentList, N_parents)
        
        # Take these parents from the population
        SelectedPopulation = Retrieve_individuals(Population, Selected)
        
        # Apply crossover / mutation
        Offspring[(N_parents*i):(N_parents*i + N_parents),:] = Apply_recombination(SelectedPopulation, MethodCrossover = MethodCrossover, CrossoverNumber = CrossoverNumber, CrossoverRate = CrossoverRate, Alpha = Alpha, MethodMutation = MethodMutation, MutationRate = MutationRate, Sigma = Sigma, Eps = Eps)

    return Offspring        

# In[Write status]

"""
Purpose:
    Write the complete status of a run after a generation
    
Inputs:
    Logpath: Path to write the logfiles in
    Population: numpy array with Current population
    Fitness: numpy array with fitness values
    Max_fitness: List with maximum fitness per generation
    Generation: Current generation number
    
Outputs:
    None
    
Version history:
    1. Kevin, 2019/09/30: Created the function
"""    

def Save_logs(Logpath, Population, Fitness, Playerlife, Max_fitness, Generation):
    
    if Generation == 0:
        
        # Save best solution
        Best = np.argmax(Fitness)
        np.savetxt(Logpath + '\\Best_solution.txt', Population[Best,:])
        
        # Save best player life value
        life = open(Logpath + '\\Best_solution_playerlife.txt', 'w')
        life.write(str(Playerlife[Best]))
        life.close()
        
        # Write current generation
        np.save(Logpath + '\\Current_gen', Population, allow_pickle = True)
        
        # Write stats
        stats = open(Logpath + '\\Stats.txt', 'w')
        stats.write('Gen,Best,Mean,Std')
        stats.write('\n' + str(Generation) + ',' + str(round(Fitness.max(),4)) + ',' + str(round(Fitness.mean(),4)) + ',' + str(round(Fitness.std(),4)))
        stats.close()
        
        # Write generation number
        gen = open(Logpath + '\\Generation.txt', 'w')
        gen.write(str(Generation))
        gen.close()
        
        # Write (unsorted) fitness values
        fit = open(Logpath + '\\Fitness.csv', 'w', newline='')
        file = csv.writer(fit, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        file.writerow(np.round(Fitness, 4))
        fit.close()
        
        # Write playerlife
        life = open(Logpath + '\\Playerlife.csv', 'w', newline='')
        file = csv.writer(life, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        file.writerow(np.round(Playerlife, 4))
        life.close()

     
    else:
        
        # Check if new maximum fitness
        if Max_fitness[-1] == max(Max_fitness):
            Best = np.argmax(Fitness)
            np.savetxt(Logpath + '\\Best_solution.txt', Population[Best,:])

            # Save best player life value
            life = open(Logpath + '\\Best_solution_playerlife.txt', 'w')
            life.write(str(Playerlife[Best]))
            life.close()
            
        # Write current generation
        np.save(Logpath + '\\Current_gen', Population, allow_pickle = True)
        
        # Append stats
        stats = open(Logpath + '\\Stats.txt', 'a')
        stats.write('\n' + str(Generation) + ',' + str(round(Fitness.max(),4)) + ',' + str(round(Fitness.mean(),4)) + ',' + str(round(Fitness.std(),4)))
        stats.close()        
        
        # Write generation number
        gen = open(Logpath + '\\Generation.txt', 'w')
        gen.write(str(Generation))
        gen.close()
        
        # Append (unsorted) fitness values
        with open(Logpath + '\\Fitness.csv', 'a', newline='') as fit:
            writer = csv.writer(fit)
            writer.writerow(np.round(Fitness, 4))
            fit.close()
            
        # Append (unsorted) playerlife values
        with open(Logpath + '\\Playerlife.csv', 'a', newline='') as life:
            writer = csv.writer(life)
            writer.writerow(np.round(Playerlife, 4))
            life.close()

    # Code to load logfiles
#    Population = np.load(Logpath + '\\Current_gen.npy')   
#    Stats = np.loadtxt(Logpath + '\\Stats.txt', skiprows=1, delimiter=',')
    
    return

# In[Island model functions]
    
"""
Purpose:
    
    
Inputs:
    
    
Outputs:
    
    
Version history:
    
"""   

def Setup_Island(Island, env, Logpath, IslandNr):
    
    # Determine number of variables
    Island["Inputs"] = env.get_num_sensors()
    Island["NN_variables"] = (Island["Inputs"] + 1) * Island["N_nodes"] + (Island["N_nodes"] + 1) * Island["Outputs"]

    # Determine number of parameters
    if Island["MethodMutation"] == 'SA_OneStep':
        Island["N_variables"] = Island["NN_variables"] + 1
    elif Island["MethodMutation"] == 'SA_NStep':
        Island["N_variables"] = 2 * Island["NN_variables"]
    else:
        Island["N_variables"] = Island["NN_variables"]
    
    # Set up mean and maximum lists
    Island["Fitness_over_time"] = []
    Island["Max_fitness_over_time"] = []
    
    # Add logpath
    Island["Logpath"] = Logpath + '\\Island' + str(IslandNr)
    os.makedirs(Island["Logpath"])
    
    return Island

"""
Purpose:
    
    
Inputs:
    
    
Outputs:
    
    
Version history:
    
""" 

def Initialise_Island(Island, env):
        
    # Generate and evaluate initial population
    Island["Generation"] = 0
    Island["Population"] = np.concatenate((np.random.normal(0, 1, [Island["Mu"], Island["NN_variables"]]), np.random.rand(Island["Mu"], (Island["N_variables"] - Island["NN_variables"]))), axis=1)
    Island["Fitness_population"], Island["Playerlife_population"] = Evaluate_population(Island["Population"], env, Island["NN_variables"])
    
    # Print average and maximum fitness
    print("Average fitness of generation 0 is: %f" %(Island["Fitness_population"].mean()))
    print("Maximum fitness of generation 0 is: %f" %(Island["Fitness_population"].max()))
    
    # Add mean and max fitness to lists
    Island["Fitness_over_time"].append(Island["Fitness_population"].mean())
    Island["Max_fitness_over_time"].append(Island["Fitness_population"].max())
    
    # Write logfiles
    Save_logs(Island["Logpath"], Island["Population"], Island["Fitness_population"], Island["Playerlife_population"], Island["Max_fitness_over_time"], 0)
    
    return Island

"""
Purpose:
    
    
Inputs:
    
    
Outputs:
    
    
Version history:
    
""" 

def Evolve_Island(Island, env, N_generations):
    
    Generation = int(np.loadtxt(Island["Logpath"] + '\\Generation.txt'))
    
    # Perform N generations
    for i in range(Generation, Generation + N_generations):
        
        # Select parents
        ParentProbabilities = Get_probabilities(Island["Fitness_population"], Island["MethodParentProb"], S = Island["S"], N_individuals = Island["Lambda"], q = Island["q"])
        ParentList = Select_individuals(ParentProbabilities, Island["MethodParentSelection"], Island["Lambda"])
        
        # Generate and evaluate offspring
        Island["Offspring"] = Get_offspring(Island["Population"], ParentList, Island["N_parents"], MethodCrossover = Island["MethodCrossover"], CrossoverNumber = Island["CrossoverNumber"], CrossoverRate = Island["CrossoverRate"], Alpha = Island["Alpha"], MethodMutation = Island["MethodMutation"], MutationRate = Island["MutationRate"], Sigma = Island["Sigma"], Eps = Island["Eps"])
        Island["Fitness_offspring"], Island["Playerlife_offspring"] = Evaluate_population(Island["Offspring"], env, Island["NN_variables"])
        
        # Select survivors, extract them from offspring and retrieve their fitness
        SurvivalProbabilities = Get_probabilities(Island["Fitness_offspring"], Island["MethodSurvivorProb"], S = Island["S"], N_individuals = Island["Mu"], q = Island["q"])
        Survivorlist = Select_individuals(SurvivalProbabilities, Island["MethodSurvivorSelection"], Island["Mu"])
        Island["Population"] = Retrieve_individuals(Island["Offspring"], Survivorlist)
        Island["Fitness_population"] = Retrieve_fitness(Island["Fitness_offspring"], Survivorlist)
        Island["Playerlife_population"] = Retrieve_fitness(Island["Playerlife_offspring"], Survivorlist)
        
        # Check if elitism applies
        Island["Population"], Island["Fitness_population"], Island["Playerlife_population"] = Apply_elitism(Island["Population"], Island["Fitness_population"], Island["Playerlife_population"], Island["Offspring"], Island["Fitness_offspring"], Island["Playerlife_offspring"], Island["Elitism"], Island["Max_fitness_over_time"], Island["Logpath"])
                
        # Print fitness statistics, add mean and max fitness to lists
        print("Average fitness of generation %i is: %f" %(i+1, Island["Fitness_population"].mean()))
        print("Maximum fitness of generation %i is: %f" %(i+1, Island["Fitness_population"].max()))
        Island["Fitness_over_time"].append(Island["Fitness_population"].mean())
        Island["Max_fitness_over_time"].append(Island["Fitness_population"].max())
        
        # Write logfles
        Save_logs(Island["Logpath"], Island["Population"], Island["Fitness_population"], Island["Playerlife_population"], Island["Max_fitness_over_time"], i+1)
    
    return Island

"""
Purpose:
    Randomly selecting nr_exchanges integer, representing individuals to exchange in the Island model
    
Inputs:
    Pop: matrix holding genes for every individual on the island
    N_exchanges: int. The number of individuals from every island to exchange
    
Outputs:
    SelectedPop: array holding integers, representing individuals to exchange in the Island model
        
Version history:
    1. Thijmen, 2019/10/11: Created the function
    2. Kevin, 2019/10/11: Cleaned the function
"""    

def Sample_exchanges(Pop, N_exchanges):
  
    # Make empty array with individuals
    N_individuals = Pop.shape[0]
    SelectedPop = np.zeros(N_exchanges, dtype = int)
    
    # Select individual in every interval
    for i in range(0, N_exchanges):
        SelectedPop[i] = int((np.random.rand() / N_exchanges + i / N_exchanges) * N_individuals)
    
    return SelectedPop

"""
Purpose:
    Exchange individuals from every island
    
Inputs:
    Island1 - Island5: Dictionary holding all relevant information on the Islands. Importantly the individuals, fitness values and player life 
    nr_exchanges: int. The number of individuals from every island to exchange
    
Outputs:
    Island1-Island5: The dictionaries for every island, where nr_exchanges individuals from every island have been migrated to another island, also the fitness values and player life statistics are transferred
    
Version history:
    1. Kevin, 2019/10/11: Created the function
"""    
def Exchange_Islands(Island1, Island2, Island3, Island4, Island5, N_exchanges):
    
    # Retrieve populations
    Pop1 = Island1["Population"]
    Pop2 = Island2["Population"]
    Pop3 = Island3["Population"]
    Pop4 = Island4["Population"]
    Pop5 = Island5["Population"]
    
    # Retrieve player life
    Playerlife1 = Island1["Playerlife_population"]
    Playerlife2 = Island2["Playerlife_population"]
    Playerlife3 = Island3["Playerlife_population"]
    Playerlife4 = Island4["Playerlife_population"]
    Playerlife5 = Island5["Playerlife_population"]
    
    # Retrieve fitness
    Fitness1 = Island1["Fitness_population"]
    Fitness2 = Island2["Fitness_population"]
    Fitness3 = Island3["Fitness_population"]
    Fitness4 = Island4["Fitness_population"]
    Fitness5 = Island5["Fitness_population"]
    
    # Putting the information in arrays for easy access
    Populations = [Pop1, Pop2, Pop3, Pop4, Pop5] #Making an array of the population matrices
    Playerlives = [Playerlife1, Playerlife2, Playerlife3, Playerlife4,Playerlife5] #Making an array of the playerlife arrays
    Fitnesses = [Fitness1, Fitness2, Fitness3, Fitness4, Fitness5] #Making an array of the fitness arrays
    
    # Retrieve population size, make empty selection matrix
    gene_size = Pop1.shape[1] #The number of genes for every individual
    SelectPop = np.zeros([5,N_exchanges],dtype=int) #Defining a numpy matrix that will hold the indices of the selected individuals to exchange for every island
    
    # Declaring matrices holding information on the individuals to exchange
    individuals = np.zeros([5,N_exchanges,gene_size]) #Defining the numpy matrix that for every island, contains all the genes of the nr_exchanges (a number, e.g. 5) individuals 
    individual_lives = np.zeros([5,N_exchanges]) #Defining the numpy matrix that for every island, contains all the genes of the nr_exchanges (a number, e.g. 5) individuals 
    individual_fitnesses = np.zeros([5,N_exchanges]) #Defining the numpy matrix that for every island, contains all the genes of the nr_exchanges (a number, e.g. 5) individuals 
    
    # Loop over every population
    for i in range(0,5):
        
        # Make selection
        SelectPop[i] = Sample_exchanges(Populations[i], N_exchanges) #Selecting nr_exchanges individuals from island i
        
        # Extract information
        individuals[i] = Retrieve_individuals(Populations[i], SelectPop[i])
        individual_lives[i] = Retrieve_fitness(Playerlives[i], SelectPop[i])
        individual_fitnesses[i] = Retrieve_fitness(Fitnesses[i], SelectPop[i])
        
        # Delete information
        Populations[i] = np.delete(Populations[i], SelectPop[i], axis=0)
        Playerlives[i] = np.delete(Playerlives[i], SelectPop[i], axis=0)
        Fitnesses[i] = np.delete(Fitnesses[i], SelectPop[i], axis=0)
            
    # Place back new populations, playerlife and fitness
    Island1["Population"] = np.vstack((Populations[0], individuals[4]))
    Island2["Population"] = np.vstack((Populations[1], individuals[0]))
    Island3["Population"] = np.vstack((Populations[2], individuals[1]))
    Island4["Population"] = np.vstack((Populations[3], individuals[2]))
    Island5["Population"] = np.vstack((Populations[4], individuals[3]))

    Island1["Playerlife_population"] = np.hstack((Playerlives[0], individual_lives[4]))
    Island2["Playerlife_population"] = np.hstack((Playerlives[1], individual_lives[0]))
    Island3["Playerlife_population"] = np.hstack((Playerlives[2], individual_lives[1]))
    Island4["Playerlife_population"] = np.hstack((Playerlives[3], individual_lives[2]))
    Island5["Playerlife_population"] = np.hstack((Playerlives[4], individual_lives[3]))    
    
    Island1["Fitness_population"] = np.hstack((Fitnesses[0], individual_fitnesses[4]))
    Island2["Fitness_population"] = np.hstack((Fitnesses[1], individual_fitnesses[0]))
    Island3["Fitness_population"] = np.hstack((Fitnesses[2], individual_fitnesses[1]))
    Island4["Fitness_population"] = np.hstack((Fitnesses[3], individual_fitnesses[2]))
    Island5["Fitness_population"] = np.hstack((Fitnesses[4], individual_fitnesses[3]))    
    
    return Island1, Island2, Island3, Island4, Island5