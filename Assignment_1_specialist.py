"""
Purpose:
    Creating a specialist EA

Version:
    1 - Initial version

Date:
    2019.09.20

Author:
    Kevin Overbeek, Thijmen van Arnhem, Chris Kostopoulos
"""

# In[Imports, setting up environment]

# Set working directory
import os
if os.getlogin() == "Kevin Overbeek":
    os.chdir('D:/Kevin Overbeek/Documents/Studie/2019-20 BA/6.1 Evolutionary Computing/Bitbucket')

# Import framework, 
import sys
sys.path.insert(0, 'evoman')
from environment import Environment
from demo_controller import player_controller

# Import standard python packages & custom made functions
import time #analysis:ignore
import numpy as np #analysis:ignore
import pandas as pd #analysis:ignore
import csv #analysis:ignore
import Assignment_functions as fn #analysis:ignore

# Make folder to write experiments to
experiment_name = 'Assignment_specialist'
if not os.path.exists(experiment_name):
    os.makedirs(experiment_name)
Logpath = os.getcwd() + '\\' + experiment_name

# initializes simulation in individual evolution mode, for single static enemy.
env = Environment(experiment_name = experiment_name,
                  enemies = [2],
                  playermode = "ai",
                  player_controller = player_controller(),
                  enemymode = "static",
                  level = 2,
                  speed = "fastest")

# Print environment state
env.state_to_log()

# In[Parameter setting]

# Choose to start a new run / load previous, choose to show best solution so far
Newrun = 'Y' # 'Y' or 'N'
Showbest = 'N' # 'Y' or 'N'

# Set start time and random seed
StartTime = time.time()  # sets time marker
Randseed = 0
np.random.seed(Randseed)

# Meta-EA parameters
MethodMutation = "Uniform"
MethodCrossover = None
MethodSurvivorProb = "Lin_rank"
MethodSurvivorSelection = "Roulette"
MethodParentProb = "Lin_rank"
MethodParentSelection = "Roulette"
Elitism = 'N'

# Adjust log path to make it EA dependent
Logpath += "\\" + MethodMutation + "_" + str(MethodCrossover) + "_" + MethodSurvivorProb + "_" + MethodSurvivorSelection + "_" + MethodParentProb + "_" + MethodParentSelection
if not os.path.exists(Logpath):
    os.makedirs(Logpath)

# EA parameters
N_generations = 25
Mu = 25
Lambda = 100
S = 1.75
N_parents = 2
MutationRate = 0.18
CrossoverRate = 0.7
CrossoverNumber = 2
Sigma = 1
Eps = 0.01
q = 3
Alpha = 0.5
    
# NN parameters
Inputs = env.get_num_sensors()
N_nodes = 10
Outputs = 5
NN_variables = (Inputs + 1) * N_nodes + (N_nodes + 1) * Outputs

# Determine number of parameters
if MethodMutation == 'SA_OneStep':
    N_variables = NN_variables + 1
elif MethodMutation == 'SA_NStep':
    N_variables = 2 * NN_variables
else:
    N_variables = NN_variables

# Set up mean and maximum lists
Fitness_over_time = []
Max_fitness_over_time = []

# In[Load current state / initialize]

if Newrun == 'N':
    
    # Try to load all stored information
    try:
        
        # Load generation number and population
        Generation = int(np.loadtxt(Logpath + '\\Generation.txt'))
        Population = np.load(Logpath + '\\Current_gen.npy')
        
        # Load last line of fitness file
        with open(Logpath + '\\Fitness.csv', 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter = ',')
            Fitness_population = pd.read_csv(csvfile, header = None).iloc[-1,:].to_numpy()
        
        # Restore average and maximum fitness value lists
        Stats = np.loadtxt(Logpath + '\\Stats.txt', skiprows=1, delimiter=',')
        
        # Check if 2 dimensional, if not force to
        if len(Stats.shape) == 1:
            Stats = Stats.reshape([1, len(Stats)])
        
        for row in range(0,Stats.shape[0]):
            Max_fitness_over_time.append(Stats[row,1])
            Fitness_over_time.append(Stats[row,2])
        
        del Stats
        
    except:
        
        # If not possible to load state, print error and initialize
        print("Not possible to load state, initializing new run")
        Newrun = 'Y'
        
if Newrun == 'Y':

    # Generate and evaluate initial population
    Generation = 0
    Population = np.concatenate((np.random.normal(0, 1, [Mu, NN_variables]), np.random.rand(Mu, (N_variables - NN_variables))), axis=1)
    Fitness_population = fn.Evaluate_population(Population, env, NN_variables)
    
    # Print average and maximum fitness
    print("Average fitness of generation 0 is: %f" %(Fitness_population.mean()))
    print("Maximum fitness of generation 0 is: %f" %(Fitness_population.max()))
    
    # Add mean and max fitness to lists
    Fitness_over_time.append(Fitness_population.mean())
    Max_fitness_over_time.append(Fitness_population.max())
    
    # Write logfiles
    fn.Save_logs(Logpath, Population, Fitness_population, Max_fitness_over_time, 0)

# In[Run generations]

# Perform N generations
for i in range(Generation, Generation + N_generations):
    
    # Select parents
    ParentProbabilities = fn.Get_probabilities(Fitness_population, MethodParentProb, S = S, N_individuals = Lambda, q = q)
    ParentList = fn.Select_individuals(ParentProbabilities, MethodParentSelection, Lambda)
    
    # Generate and evaluate offspring
    Offspring = fn.Get_offspring(Population, ParentList, N_parents, MethodCrossover = MethodCrossover, CrossoverNumber = CrossoverNumber, CrossoverRate = CrossoverRate, Alpha = Alpha, MethodMutation = MethodMutation, MutationRate = MutationRate, Sigma = Sigma, Eps = Eps)
    Fitness_offspring = fn.Evaluate_population(Offspring, env, NN_variables)
    
    # Select survivors, extract them from offspring and retrieve their fitness
    SurvivalProbabilities = fn.Get_probabilities(Fitness_offspring, MethodSurvivorProb, S = S, N_individuals = Mu, q = q)
    Survivorlist = fn.Select_individuals(SurvivalProbabilities, MethodSurvivorSelection, Mu)
    Population = fn.Retrieve_individuals(Offspring, Survivorlist)
    Fitness_population = fn.Retrieve_fitness(Fitness_offspring, Survivorlist)
    
    # Check if elitism applies
    Population, Fitness_population = fn.Apply_elitism(Population, Fitness_population, Offspring, Fitness_offspring, Elitism, Max_fitness_over_time, Logpath)
            
    # Print fitness statistics, add mean and max fitness to lists
    print("Average fitness of generation %i is: %f" %(i+1, Fitness_population.mean()))
    print("Maximum fitness of generation %i is: %f" %(i+1, Fitness_population.max()))
    Fitness_over_time.append(Fitness_population.mean())
    Max_fitness_over_time.append(Fitness_population.max())
    
    # Write logfles
    fn.Save_logs(Logpath, Population, Fitness_population, Max_fitness_over_time, i+1)
    
# In[Run best solution]            

# Beware: crashes after it completes, deletes all variables in memory
if Showbest == 'Y':
    Best = np.loadtxt(Logpath + '\\Best_solution.txt')
    env.update_parameter('speed','normal')
    fn.Evaluate_population(Best.reshape(1, len(Best)), env, NN_variables)