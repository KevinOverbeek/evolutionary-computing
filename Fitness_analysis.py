"""
Purpose:
    Evaluating the results of the specialist EA

Version:
    1 - Initial version

Date:
    2019.10.04

Author:
    Kevin Overbeek, Thijmen van Arnhem, Chris Kostopoulos
"""


# In[Imports & Plotting]

import os
import pandas as pd #analysis:ignore
import matplotlib.pyplot as plt
import numpy as np #analysis:ignore
from scipy import stats

# Set working directory
if os.getlogin() == "Kevin Overbeek":
    os.chdir('D:/Kevin Overbeek/Documents/Studie/2019-20 BA/6.1 Evolutionary Computing/Bitbucket')

# Avoid plots are being closed when ran in profiler
plt.ioff()

# Set up empty directories / lists
Stats = {}
Fitness = {}
Score = []

# Loop over seeds / enemies
Seeds = [0,1,2,3,4,5,6,7,8,9]
Enemies = [2,6,8]

# Make empty matrix with best fitness values for every enemy / run
Best = np.zeros([len(Enemies), len(Seeds)])
     
# Starting loop to test different parameter settings
experiment_name = 'Assignment_specialist'

for enemy in Enemies:
    for Randseed in Seeds:

            # Determine path where results are stored
            Logpath = os.getcwd() + '\\' + experiment_name + '\\EA1\\Enemy_' + str(enemy) + '_Seed_' + str(Randseed)
            
            # Load stats, fill best solution matrix
            StatsTemp = np.loadtxt(Logpath + '\\Stats.txt', skiprows=1, delimiter=',')
            Best[Enemies.index(enemy), Seeds.index(Randseed)] = StatsTemp[:,1].max()
            
            # Save stats and fitness values
            Stats["Enemy" + str(enemy) + "Seed" + str(Randseed)] = StatsTemp
            Fitness["Enemy" + str(enemy) + "Seed" + str(Randseed)] = pd.read_csv(Logpath + "\\Fitness.csv", header=None)

# Concatenate different runs for every enemy
for enemy in Enemies:
    
    # Make empty matrix
    DataTemp = pd.DataFrame()
    
    # Loop over every run of the algorithm
    for Randseed in Seeds:
        
        # Append data to matrix such that every row is a generation
        DataTemp = pd.concat([DataTemp, Fitness["Enemy" + str(enemy) + "Seed" + str(Randseed)]], axis=1)
    
    # Calculate mean and standard deviation for plot
    Mean = DataTemp.mean(axis=1)
    Std = DataTemp.std(axis=1)
    
    # Make plot
    fig, ax1 = plt.subplots()
    ax1.plot(Mean, color="tab:blue")
    ax1.set_title("Mean and Standard Deviation of Fitness per Generation \n Enemy " + str(enemy))
    ax1.set_xlabel("Generation", fontsize='large')
    ax1.set_ylabel("Fitness", fontsize='large')
    ax2 = ax1.twinx()
    ax2.plot(Std, color="tab:red")
    ax2.set_ylabel("Standard Deviation", fontsize='large')
    fig.tight_layout()
    fig.legend(("Mean", "Standard deviation"), loc=(0.5, 0.5), fontsize = 'large')
    plt.show()
    
    # Store fitness matrix
    Fitness["Enemy" + str(enemy)] = DataTemp
    
# In[Significance tests]
    
# Rename best solutions from baseline model
BestEA1 = Best
    
# Load best solutions of advanced model
Logpath = os.getcwd() + '\\' + experiment_name 
BestEA2 = np.loadtxt(Logpath + '\\best.txt')

# Compare every enemy with boxplot and t-test
for i in range(len(Enemies)):
    enemy = Enemies[i]
    
    # Make boxplot
    plt.boxplot([BestEA1[i,:],BestEA2[i,:]])
    plt.title("Boxplots for the maximum fitness per run \n Enemy " + str(enemy))
    plt.xlabel("EA number", fontsize = 'large')
    plt.ylabel("Fitness", fontsize = 'large')
    plt.show()
    
    # Perform t-test
    t = (BestEA2[i,:].mean() - BestEA1[i,:].mean()) / np.sqrt((BestEA1[i,:].var() / BestEA1.shape[1]) + (BestEA2[i,:].var() / BestEA2.shape[1]))
    df = 2*BestEA1.shape[1] - 2
    p = 1 - stats.t.cdf(t,df=df)
    
    # Print outcomes
    print("For enemy %d, the t value is equal to %f with a p value of %f" % (enemy, t, p))
